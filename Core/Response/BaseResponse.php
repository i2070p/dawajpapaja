<?php
/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 11:04
 */

namespace Core\Response;


abstract class BaseResponse
{
    private $statusCode;

    /**
     * BaseResponse constructor.
     * @param $statusCode
     */
    public function __construct($statusCode = 200)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    protected abstract function prepare();

    public final function generate() {

        $this->prepare();

        http_response_code($this->statusCode);
    }
}