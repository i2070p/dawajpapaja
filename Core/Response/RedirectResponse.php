<?php
/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 11:04
 */

namespace Core\Response;

class RedirectResponse extends BaseResponse
{
    private $url;

    /**
     * RedirectResponse constructor.
     * @param $url
     */
    public function __construct($url)
    {
        parent::__construct(302);
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function prepare()
    {
        header("Location: " . $this->url);
    }
}