<?php
/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 11:04
 */

namespace Core\Response;

class Response extends BaseResponse
{
    private $view;

    private $parameters;

    /**
     * Response constructor.
     * @param $view
     * @param $parameters
     */
    public function __construct($view, array $parameters = [], $statusCode = 200)
    {
        parent::__construct($statusCode);
        $this->view = $view;
        $this->parameters = $parameters;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param mixed $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param mixed $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;
    }


    /**
     * @param $key
     * @return mixed
     */
    public function getParameter($key)
    {
        return $this->parameters[$key];
    }

    public function prepare()
    {
        extract($this->parameters);

        include __DIR__ . "/../../View/" . $this->view;

    }
}