<?php
/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 11:04
 */

namespace Core\Response;

class ImageResponse extends BaseResponse
{
    private $path;

    /**
     * BaseResponse constructor.
     * @param $path
     */
    public function __construct($path, $statusCode = 200)
    {
        parent::__construct($statusCode);
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    public function prepare()
    {
        $fp = fopen($this->path, 'rb');

        header("Content-Type: " . mime_content_type($this->path));
        header("Content-Length: " . filesize($this->path));

        fpassthru($fp);

    }
}