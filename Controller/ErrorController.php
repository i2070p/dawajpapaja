<?php

/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 01:19
 */

namespace Controller;

use Core\Response\ImageResponse;
use Core\Response\Response;

class ErrorController extends BaseController
{
    public function error404()
    {
        return new ImageResponse('papaje/135508589258.png', 404);
    }

    public function errorMissingParameters($names)
    {
        return new Response(
            'Error/missing_parameters.html',
            [
                'parameterNames' => $names
            ],
            400
        );
    }
}