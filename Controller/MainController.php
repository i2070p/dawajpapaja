<?php

/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 01:19
 */

namespace Controller;

use Core\Response\ImageResponse;
use Core\Response\RedirectResponse;
use Core\Response\Response;

class MainController extends BaseController
{
    const DIR = 'papaje';

    public function losujPapaja()
    {
        $papaje = $this->getDirContent(self::DIR);

        $randomId = rand(0, count($papaje) - 1);

        return new RedirectResponse("papaj?filename=" . $papaje[$randomId]);
    }


    public function papaj($filename)
    {
        $path = self::DIR . '/' . $filename;

        if (!file_exists($path)) {
            return new Response(
                'Main/message.html',
                [
                    'title' => 'super wiadomosc kurwo',
                    'message' => 'NIE MA TAKIEGO PAPIEZA'
                ],
                404
            );
        }

        return new ImageResponse($path);

    }

    public function dawajPapaja($phrase)
    {
        $papaje = $this->getDirContent(self::DIR);

        $id = $this->getNumberFromHash(md5($phrase), count($papaje) - 1);

        return new ImageResponse(self::DIR . '/' . $papaje[$id]);

    }

    private function getDirContent($dir)
    {
        $result = scandir($dir);

        foreach (['.', '..'] as $excluded) {
            unset($result[array_search($excluded, $result)]);
        }

        return array_values($result);
    }

    private function getNumberFromHash($hash, $maxValue)
    {
        $result = 0;

        for ($i = 0; $i < strlen($hash); $i++) {
            $result += pow(ord($hash[$i]), 2);
        }

        return $result % $maxValue;
    }
}