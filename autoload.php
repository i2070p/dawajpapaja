<?php
/**
 * Created by PhpStorm.
 * User: meti
 * Date: 2016-10-23
 * Time: 13:48
 */

$directories = [
    'Controller' => "*Controller.php",
    'Core/Response' => "*Response.php"
];

foreach ($directories as $directory => $pattern) {
    if ($files = glob("$directory/$pattern")) {
        foreach ($files as $file) {
            require_once $file;
        }
    }
}