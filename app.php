<?php

require_once "autoload.php";
require_once "routing.php";

final class FrontController
{
    private $url;

    private $routes;

    /** @var \Controller\ErrorController */
    private $errorController;

    /**
     * FrontController constructor.
     */
    public function __construct($url, $routes)
    {
        $this->url = $url;
        $this->routes = $routes;
        $this->errorController = new \Controller\ErrorController();
    }

    private function callMethod($object, $method, $args)
    {
        if (method_exists($object, $method)) {

            $arguments = [];
            $reflectionMethod = new ReflectionMethod($object, $method);

            foreach ($reflectionMethod->getParameters() as $arg) {
                if (isset($args[$arg->name])) {
                    $arguments[$arg->name] = $args[$arg->name];
                } else if ($arg->isDefaultValueAvailable()) {
                    $arguments[$arg->name] = $arg->getDefaultValue();
                } else {
                    $arguments[$arg->name] = null;
                }
            }

            return call_user_func_array([$object, $method], $arguments);
        }
        return null;
    }

    private function prepareResponse()
    {
        if ($actions = array_values(preg_grep("/^" . $this->url . "$/", array_keys($this->routes)))) {

            $action = $this->routes[current($actions)];

            $method = $action['method'];
            $controllerName = $action['controller'];

            $parameterNames = [];

            if (isset($action['required_parameters'])) {

                $missingParameterNames = [];

                foreach ($action['required_parameters']['GET'] as $required) {
                    if (!isset($_GET[$required])) {
                        $missingParameterNames[] = $required;
                    } else {
                        $parameterNames[$required] = $_GET[$required];
                    }
                }

                if ($missingParameterNames) {
                    return $this->errorController->errorMissingParameters($missingParameterNames);
                }

            }

            $controllerClass = "\\Controller\\" . $controllerName . "Controller";
            $controller = new $controllerClass;

            return $this->callMethod($controller, $method, $parameterNames);

        }

        return null;
    }

    public function handle()
    {
        $response = $this->prepareResponse();

        if (!$response) {
            $response = $this->errorController->error404();
        }

        $response->generate();
    }
}

$frontController = new FrontController(
    preg_replace("/\/" . basename(__FILE__) . "\//", "", strtok($_SERVER["REQUEST_URI"], '?')),
    Routing::getRoutes()
);

$frontController->handle();