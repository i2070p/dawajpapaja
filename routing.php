<?php

final class Routing
{
    private static $routes = [

        'dawaj_papaja' => [
            'controller' => 'Main',
            'method' => 'dawajPapaja',
            'required_parameters' => [
                'GET' => [
                    'phrase'
                ]
            ],
        ],

        'losuj_papaja' => [
            'controller' => 'Main',
            'method' => 'losujPapaja',
        ],

        'papaj' => [
            'controller' => 'Main',
            'method' => 'papaj',
            'required_parameters' => [
                'GET' => [
                    'filename'
                ]
            ],
        ]

    ];

    public static function getRoutes()
    {
        return self::$routes;
    }
}
